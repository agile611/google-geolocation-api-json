package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import utils.Localizacion;
import utils.json.JSONArray;
import utils.json.JSONObject;

public class GoogleLocationApi {
	public static final long TAG_LONG = 0x4c51fa67372d11L;

	public static JSONObject getJSONLocalization() {

		JSONObject jsonString = new JSONObject();
		try {

			JSONObject map = new JSONObject();
			map.put("mobileCountryCode", 310);
			map.put("mobileNetworkCode", 260);
			map.put("locationAreaCode", 40495);
			map.put("signalStrength", -79);
			map.put("cellId", 39627456);
			map.put("age", 0);

			JSONArray array = new JSONArray();
			array.put(0, map);
			jsonString.put("cellTowers", array);

			jsonString.put("homeMobileCountryCode", 310);
			jsonString.put("homeMobileNetworkCode", 260);

			jsonString.put("radioType", "gsm");
			jsonString.put("carrier", "T-Mobile");

			System.out.println("!!!!!!!! JSON A ENVIAR=>"
					+ jsonString.toString().trim());

		} catch (Exception e) {
			System.out
					.println("EXCEPCION AL CONSTRUIR JSON PARA MANDAR CON GOOGLE API=>"
							+ e.getMessage());
			return null;
		}
		System.out.println("getJSONLocalization jsonString " + jsonString);
		return jsonString;
	}

	public static Localizacion getLocationFromJsonResponse(String json) {

		try {
			System.out.println("getLocationFromJsonResponse " + json.trim());
			JSONObject jsonResponse = new JSONObject(json.trim());
			
			//Latitude and Longitude
			JSONObject location = jsonResponse.getJSONObject("location");
			Localizacion info = new Localizacion();
			info.setFromGPS(false);
			info.setLatitude(Double.parseDouble(location.get("lat").toString()));
			info.setLongitude(Double
					.parseDouble(location.get("lng").toString()));
			
			//Radius accuracy
			double accuracyValue = Double.parseDouble(jsonResponse.get("accuracy")
					.toString());
			info.setHorAccuracy((float) (accuracyValue));
			
			return info;
		} catch (Exception e) {
			System.out
					.println("EXCEPCION AL PARSEAR EL RECIBIDO CON GOOGLE API=>"
							+ e.getMessage());
			return null;
		}
	}

	public static HttpResponse sendJsonToGoogleGeolocationAPI(JSONObject json) {
		DefaultHttpClient httpClient = new DefaultHttpClient();

		try {
			HttpPost request = new HttpPost(
					"https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBDBKX-C0Uh2C2kdbqAUzLdZRN7jSoJbjE");

			StringEntity entity = new StringEntity(json.toString());
			entity.setContentType("application/json");
			request.setHeader("Content-Type", "application/json");
			request.setEntity(entity);
			HttpResponse response = httpClient.execute(request);
			return response;
		} catch (Exception e) {
			System.out
					.println("EXCEPCION sendJsonToGoogleGeolocationAPI de GOOGLE API=>"
							+ e.getMessage());
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		return null;
	}

	private static void Pedir() {
		try {
			JSONObject json = GoogleLocationApi.getJSONLocalization();
			try {
				HttpResponse retorno = sendJsonToGoogleGeolocationAPI(json);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(retorno.getEntity().getContent(),
								"UTF-8"));
				StringBuilder builder = new StringBuilder();
				for (String line = null; (line = reader.readLine()) != null;) {
					builder.append(line).append("\n");
				}
				System.out.println("builder.toString() " + builder.toString());

				Localizacion info = GoogleLocationApi
						.getLocationFromJsonResponse(builder.toString());
				// Uncomment when attached to
				// to the real Blackberry agent
				// milocator.asignarLocation(info);
				System.out
						.println("OBTENGO LOCALIZAZION POR GOOGLE WEBSERVICE");
				System.out.println("INFO=>" + info.getLatitude() + ","
						+ info.getLongitude() + "  RADIO=>"
						+ info.getHorAccuracy());
			} catch (Exception e) {
				System.out.println("ex en google api=>" + e.getMessage());
			}
		} catch (Exception lex) {
			System.out.println("ex en google api=>" + lex.getMessage());
			return;
		}
	}

	public static void main(String[] args) {
		Pedir();
	}

}
