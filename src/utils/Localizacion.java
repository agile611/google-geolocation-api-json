package utils;

import javax.microedition.location.Location;
import javax.microedition.location.QualifiedCoordinates;

public class Localizacion {
    private double latitude;
    private double longitude;
    private boolean fromGPS;
    private float horAccuracy;
    private float verAccuracy;
   
    public Localizacion() {
       
    }

    public Localizacion(Location location){
    	QualifiedCoordinates q = location.getQualifiedCoordinates();
		this.setLongitude(q.getLongitude());
		this.setLatitude(q.getLatitude());
		this.setHorAccuracy(q.getHorizontalAccuracy());
    }
    
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public boolean isFromGPS() {
		return fromGPS;
	}

	public void setFromGPS(boolean fromGPS) {
		this.fromGPS = fromGPS;
	}

	public float getHorAccuracy() {
		return horAccuracy;
	}

	public void setHorAccuracy(float horAccuracy) {
		this.horAccuracy = horAccuracy;
	}

	public float getVerAccuracy() {
		return verAccuracy;
	}

	public void setVerAccuracy(float verAccuracy) {
		this.verAccuracy = verAccuracy;
	}
    
    
    
    
    
    
    
}